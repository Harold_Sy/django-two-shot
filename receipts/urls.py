from receipts.views import receipt_view
from django.urls import path

urlpatterns = [
    path("", receipt_view, name="home"),
]
